<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

function efgcp_render_sermon_output() {
    
    $html  =    '';
    $html .=    '<article id="sermon-' . get_the_id() . '" class="sermon vc_col-lg-12">';
    $html .=        '<div class="sermon-item">';
    $html .=            '<div class="sermon-inner">';
    $html .=                '<div class="sermon-details">';
    $html .=                    '<div class="sermon-play-pause">';
    $html .=                        '<script type="text/javascript">';
    $html .=                            'jQuery(function($){

                                            $("audio").mediaelementplayer({
                                                features: ["playpause","progress"],
                                                enableProgressTooltip: false,
                                                iPhoneUseNativeControls: true,
                                                iPadUseNativeControls: true,
                                                AndroidUseNativeControls: true,
                                                pauseOtherPlayers: true,
                                                enableKeyboard: false,
                                                autosizeProgress : false,
                                                defaultAudioHeight: 15,
                                            });
                                        });';
    $html .=                        '</script>';
    $html .=                    '</div>';
    $html .=                    '<div class="sermon-info">';
    $html .=                        '<h4 class="sermon-title">' . mb_strimwidth ( get_the_title(), 0, 70, '...') . '</h4>';
    $html .=                            '<div class="sermon-meta">';
    $html .=                                '<div class="sermon-series">' . get_the_sermon_series() . '</div>';
    $html .=                                '<div class="sermon-speaker">' . get_the_sermon_speaker() . '</div>';
    $html .=                                '<div class="sermon-date">' . strip_tags ( get_the_date('d. F Y') ) . '</div>';
    $html .=                            '</div>';
    $html .=                        '</div>';
    $html .=                        '<div class="sermon-description">';
    $html .=                            mb_strimwidth ( get_the_content(), 0, 150, '...');
    $html .=                        '</div>';
    $html .=                        '<div class="sermon-attachments">';
    $html .=                            '<div class="download-record">';
    $html .=                                '<a class="download" href="' . get_the_sermon_record_url() . '" download>';
    $html .=                                    '<div class="icon"></div>';
    $html .=                                    '<div class="text">Aufnahme</div>';
    $html .=                                '</a>';
    $html .=                            '</div>';
    $html .=                        '<div class="download-docs">';
    $html .=                            '<a class="download" href="' . get_the_sermon_handout_url() . '" download>';
    $html .=                                '<div class="icon"></div>';
    $html .=                                '<div class="text">Handout</div>';
    $html .=                            '</a>';
    $html .=                        '</div>';
    $html .=                    '</div>';
    $html .=                '</div>';
    $html .=                '<div class="sermon-player">';
    $html .=                    '<audio id="sermon-record" width="750px;" src="' . get_the_sermon_record_url() . '" type="audio/mp3">';
    $html .=                        /*'<source src="' . get_the_sermon_record_url() . '" type="audio/mp3">';*/
    $html .=                        'Your user agent does not support the HTML5 Audio element.';
    $html .=                    '</audio>';
    $html .=                '</div>';
    $html .=            '</div>';
    $html .=        '</div>';
    $html .=    '</article>';
    
    return $html;
}