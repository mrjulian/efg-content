<?php 

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

function efgcp_render_person_output() {
    
    $output  =  '';
    $output .=  '<article id="item-' .  get_the_id() . '" class="person vc_col-sm-4 vc_col-md-3 vc_col-lg-3">';
    $output .=      '<div class="column-item leader-card" id="post-' .  get_the_ID() . '">';
    $output .=          '<div class="column-image">';
    $output .=              has_post_thumbnail() ? get_the_post_thumbnail() : '';
    $output .=          '</div>';
    $output .=          '<div class="column-content">';
    $output .=              '<h4 class="column-title">' . get_the_title() .'</h4>';
    $output .=              '<div class="column-details"><p>' . get_the_person_position() . '</p></div>';
    $output .=          '</div>';
    $output .=      '</div>';
    $output .=  '</article>';
    
    if ( ! empty ( $output ) )
        return $output;
}