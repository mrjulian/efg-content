<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

function efgcp_render_magazine_output () {
    
    $output  =  '';
    $output .=  '<article id="item-' . get_the_id() . '" class="magazine vc_col-sm-6 vc_col-md-4 vc_col-lg-3">';
    $output .=      '<a class="column-item" id="post-' . get_the_ID() . '" href="' . get_the_newsletter_url() . '">';
    $output .=          '<div class="column-image">';
    $output .=              has_post_thumbnail() ? get_the_post_thumbnail() : '';
    $output .=          '</div>';
    $output .=          '<div class="column-content">';
    $output .=              '<h4 class="column-title">' . get_the_title() .'</h4>';
    $output .=          '</div>';
    $output .=      '</a>';
    $output .=  '</article>';
    
    return $output;
}