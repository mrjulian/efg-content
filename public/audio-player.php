<?php

if ( ! defined ( 'ABSPATH' ) ) exit;

function efgcp_render_audio_player() {
    
    $output  =  '';
    $output .=  '<audio id="sermon_record' . ( ! empty ($loop) ) ? $loop : '' . '">';
    $output .=      '<source src="" type="audio/mp3">';
    $output .=      '<source src="" type="audio/ogg; codecs=vorbis">';
    $output .=      'Your user agent does not support the HTML5 Audio element.';
    $output .=  '</audio>';
    $output .=  '<button type="button" onclick="aud_play_pause()">Play/Pause</button>';
    $output .=  '<script>
                    "use strict";
                    var sermonid = ' . get_the_id() . '
                    function aud_play_pause() {
                        var sermon_record = document.getElementById("sermon_record");
                        if (sermon_record.paused) {
                            sermon_record.play();
                        }
                        else {
                            sermon_record.pause();
                        }
                    }
                </script>';
    
    return $output;
}