<?php

if ( ! defined ( 'ABSPATH' ) ) exit;

/**
 * Register custom post type 'sermon'.
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_register_sermons () {
    
    $labels = array(
        'name'                => _x( 'Predigten', 'Post Type General Name', 'efgcp' ),
        'singular_name'       => _x( 'Predigt', 'Post Type Singular Name', 'efgcp' ),
        'menu_name'           => __( 'Predigten', 'efgcp' ),
        'name_admin_bar'      => __( 'Predigt', 'efgcp' ),
        'all_items'           => __( 'Alle ' . 'Predigten', 'efgcp' )
    );
    
    $rewrite = array(
        'slug'                => 'predigten',
        'with_front'          => true,
        'pages'               => true,
        'feeds'               => false
    );
    
    $args = array(
        'label'               => __( 'Predigten', 'efgcp' ),
        'labels'              => $labels,
        'rewrite'             => $rewrite,
        'supports'            => array ( 'title', 'editor', 'thumbnail' ),
        'taxonomies'          => array ( 'sermon_series', 'sermon_speaker' ),
        'capability_type'     => array ( 'sermon' , 'sermons' ),
        'map_meta_cap'        => true,
        'public'              => true,
        'menu_position'       => 20,
        'menu_icon'           => 'dashicons-microphone',
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'show_ui'             => true,
        'hierarchical'        => false,
        'can_export'          => true,
        'has_archive'         => true,	
        'exclude_from_search' => false,
        'publicly_queryable'  => true
    );
    
    register_post_type( 'sermon', $args );
}

add_action( 'init', 'efgcp_register_sermons', 11 );

/**
 * Register sermon taxonomy 'sermon_series'.
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_register_tax_sermon_series() {
    
    $labels = array(
        'name'          => _x( 'Predigtreihen', 'Taxonomy General Name', 'efgcp' ),
        'singular_name' => _x( 'Predigtreihe', 'Taxonomy Singular Name', 'efgcp' ),
        'menu_name'     => __( 'Predigtreihen', 'efgcp' ),
        'all_items'     => __( 'Alle ' . 'Predigtreihen', 'efgcp' ),
        'add_new_item'  => __( 'Neue Predigtreihe erstellen', 'efgcp' )
    );
    
    $capabilities = array(
        'manage_terms' => 'manage_series',
        'edit_terms'   => 'edit_series',
        'delete_terms' => 'delete_series',
        'assign_terms' => 'assign_series'
    );
    
    $rewrite = array(
        'slug'         => 'predigtreihen',
        'walk_dirs'    => false,
        'with_front'   => true,
        'hierarchical' => true
    );
    
    $args = array(
        'labels'            => $labels,
        'rewrite'           => $rewrite,
        'capabilities'      => $capabilities,
        'hierarchical'      => true,
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false
    );

    register_taxonomy( 'sermon_series', 'sermon', $args);
}

add_action( 'init', 'efgcp_register_tax_sermon_series', 12 );

/**
 * Register sermon taxonomy 'sermon_speaker'.
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_register_tax_sermon_speaker() {
    
    $labels = array(
        'name'          => _x( 'Referenten', 'Taxonomy General Name', 'efgcp' ),
        'singular_name' => _x( 'Referent', 'Taxonomy Singular Name', 'efgcp' ),
        'menu_name'     => __( 'Referenten', 'efgcp' ),
        'all_items'     => __( 'Alle ' . 'Referenten', 'efgcp' ),
        'add_new_item'  => __( 'Neuen Referenten erstellen', 'efgcp' )
    );
    
    $capabilities = array(
        'manage_terms'  => 'manage_speakers',
        'edit_terms'    => 'edit_speakers',
        'delete_terms'  => 'delete_speakers',
        'assign_terms'  => 'assign_speakers'
    );
    
    $rewrite = array(
        'slug'                       => 'referent',
        'walk_dirs'                  => false,
        'with_front'                 => true,
        'hierarchical'               => true
    );
    
    $args = array(
        'labels'            => $labels,
        'rewrite'           => $rewrite,
        'capabilities'      => $capabilities,
        'hierarchical'      => true,
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false
    );

    register_taxonomy( 'sermon_speaker', 'sermon', $args );
}

add_action( 'init', 'efgcp_register_tax_sermon_speaker', 13 );

/**
 * Register sermon taxonomy 'sermon_topic'.
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_register_tax_sermon_topic() {
    
    $labels = array(
        'name'          => _x( 'Thema', 'Taxonomy General Name', 'efgcp' ),
        'singular_name' => _x( 'Thema', 'Taxonomy Singular Name', 'efgcp' ),
        'menu_name'     => __( 'Themen', 'efgcp' ),
        'all_items'     => __( 'Alle ' . 'Themen', 'efgcp' ),
        'add_new_item'  => __( 'Neues Thema erstellen', 'efgcp' )
    );
    
    $capabilities = array(
        'manage_terms'  => 'manage_speakers',
        'edit_terms'    => 'edit_speakers',
        'delete_terms'  => 'delete_speakers',
        'assign_terms'  => 'assign_speakers'
    );
    
    $rewrite = array(
        'slug'                       => 'predigtthemen',
        'walk_dirs'                  => false,
        'with_front'                 => true,
        'hierarchical'               => true
    );
    
    $args = array(
        'labels'            => $labels,
        'rewrite'           => $rewrite,
        'capabilities'      => $capabilities,
        'hierarchical'      => true,
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false
    );

    register_taxonomy( 'sermon_topic', 'sermon', $args );
}

add_action( 'init', 'efgcp_register_tax_sermon_topic', 14 );

/**
 * Add user role for managing sermons and sermon taxonomies.
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_add_sermon_user_role () {
    
    $result = add_role ( 'sermon_editor', __( 'Mitarbeiter (Predigten)' ), array (
            'read' => true,
            'edit_files' => true,
            'upload_files' => true
    ) );
}

add_action( 'admin_init' , 'efgcp_add_sermon_user_role' , 900 );

/**
 * Assign common sermon capabilities to user roles.
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_assign_common_sermon_permission () {
    
    $roles = array ( 'sermon_editor' , 'editor' , 'administrator' );
    
    foreach ( $roles as $the_role ) {
        
        /* Get current capabilities */
        $role = get_role ( $the_role );
        
        /* Assign new capabilities */
        $role->add_cap( 'read' );
        $role->add_cap( 'read_private_sermons' );
        $role->add_cap( 'edit_sermons' );
        $role->add_cap( 'edit_others_sermons' );
        $role->add_cap( 'edit_private_sermons' );
        $role->add_cap( 'edit_published_sermons' );
        $role->add_cap( 'publish_sermons' );
        $role->add_cap( 'manage_series');
        $role->add_cap( 'edit_series');
        $role->add_cap( 'assign_series');
        $role->add_cap( 'manage_speakers');
        $role->add_cap( 'edit_speakers');
        $role->add_cap( 'assign_speakers');
    }
}

add_action( 'admin_init' , 'efgcp_assign_common_sermon_permission' , 901 );

/**
 * Assign restricted sermon capabilities to user roles.
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_assign_restricted_sermon_permission () {
    
    $roles = array ( 'editor' , 'administrator' );
    
    foreach ( $roles as $the_role ) {    
        
        /* Get current capabilities */
        $role = get_role ( $the_role );
        
        /* Assign new capabilities */
        $role->add_cap( 'delete_sermons' );
        $role->add_cap( 'delete_others_sermons' );
        $role->add_cap( 'delete_private_sermons' );
        $role->add_cap( 'delete_published_sermons' );
        $role->add_cap( 'delete_series');
        $role->add_cap( 'delete_speakers');
    }
}

add_action( 'admin_init' , 'efgcp_assign_restricted_sermon_permission' , 902 );

/**
 * Alter default sermon query by using advanced sermon filter.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function efgcp_sermon_alter_query( $query ) {
    
    $tax_query = array();
    
    if ( isset ( $_GET['series'] ) ) {

        $series = array (
            'taxonomy' => 'sermon_series',
            'field'    => 'id',
            'terms'    => $_GET['series']
        );
        
        array_push ( $tax_query, $series );
    }
    
    if ( isset ( $_GET['speaker'] ) ) {
        
        $speakers = array (
            'taxonomy' => 'sermon_speaker',
            'field'    => 'id',
            'terms'    => $_GET['speaker']
        );
        
        array_push ( $tax_query, $speakers );
    }
    
    if ( ! empty ( $tax_query ) ) {
        
        if ( isset( $query->query['post_type'] ) ) {
            
            if ( $query->query_vars['post_type'] != 'nav_menu_item' ) {

                $query->set( 'tax_query', $tax_query );
            }
        }
    }
}

add_action('parse_query', 'efgcp_sermon_alter_query');

/**
 * Expand searchable variables. Required by function 'efgcp_sermon_filter_advanced'.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function efgcp_query_vars ( $vars ) {

    array_push( $vars, 'series', 'speaker' );
    return $vars;
}

add_filter('query_vars', 'efgcp_query_vars');

/**
 * Check in checkbox if used. Required by function 'efgcp_sermon_filter_advanced'.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function efgcp_checkbox_state ( $array, $value ) {

    if ( ! empty ( $array ) && in_array ( $value, $array ) )
        return 'checked';
}

/**
 * Returns checkboxes for filtering sermons.
 *
 * @since       1.0.0
 * @version     1.0.0
 * @requires    efgcp_query_vars, efgcp_checkbox_state
 */
function efgcp_sermon_filter_advanced ( $atts ) {
    
    extract ( shortcode_atts (
        array(
            'sticky' => '',
        ),
        $atts )
    );
    
    $current_series  = get_query_var('series');
    $current_speaker = get_query_var('speaker');
    
    $series = get_terms( 'sermon_series', 'orderby=name');
    $speakers = get_terms( 'sermon_speaker', 'orderby=name');
    
    if ( $series || $speakers ) {

        $output = '<form class="post-filter">';

        if ($series ) {

            $output .= '<h4 class="options-title">Predigtreihen</h4>';
            $output .= '<ul class="option-list">';

            foreach ( $series as $item ) {
                $output .= '<li>';
                $output .= sprintf('<input type="checkbox" name="series[]" id="option-'.$item->term_id.'" value="'.$item->term_id.'"' . efgcp_checkbox_state( $current_series, $item->term_id ) . ' />');
                $output .= sprintf('<label for="option-'.$item->term_id.'">' . $item->name . '</label>');
                $output .= '</li>';
            }

            $output .= '</ul>';
        }

        if ( $speakers ) {

            $output .= '<h4 class="options-title">Referenten</h4>';
            $output .= '<ul class="option-list">';

            foreach ( $speakers as $item ) {

                $output .= '<li>';
                $output .= sprintf('<input type="checkbox" name="speaker[]" id="option-'.$item->term_id.'" value="'.$item->term_id.'"' . efgcp_checkbox_state( $current_speaker, $item->term_id ) . ' />');
                $output .= sprintf('<label for="option-'.$item->term_id.'">' . $item->name . '</label>');
                $output .= '</li>';
            }

            $output .= '</ul>';
        }
        
        $output .= '<button type="submit">Filter</button>';
        
        return $output;
    }
}

add_shortcode('sermon_filter_advanced', 'efgcp_sermon_filter_advanced');

/**
 * Returns assigned sermon series.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_sermon_series() {
        
    // Get all terms
    $terms = get_terms( 'sermon_series' );
    
    // Start output
    $output = '<ul class="sermon-terms">';
    
    foreach ( $terms as $term ) {
         
        // The $term is an object, so we don't need to specify the $taxonomy.
        $term_link = get_term_link( $term );
        
        // If there was an error, continue to the next term.
        if ( is_wp_error( $term_link ) ) {
            continue;
        }
        
        // We successfully got a link. Print it out.
        $output .= '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
    }
    
    $output .= '</ul>';
    
    if ( ! empty ( $output ) )
        return $output;
}

/**
 * Returns assigned sermon series.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_sermon_terms($taxonomy) {
        
    // Get all terms
    $terms = get_the_terms( get_the_ID(), $taxonomy);
    
    // Start output
    $output = '<ul class="sermon-terms">';
    
    foreach ( $terms as $term ) {
        
        $term_link = $null;
        
        // The $term is an object, so we don't need to specify the $taxonomy.
        $term_link = get_term_link( $term );
        
        // If there was an error, continue to the next term.
        if ( is_wp_error( $term_link ) ) {
            continue;
        }
        
        // We successfully got a link. Print it out.
        $output .= '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
    }
    
    $output .= '</ul>';
    
    if ( ! empty ( $output ) )
        return $output;
    
}

/**
 * Returns assigned sermon speaker.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_sermon_speaker() {
    
    $postid = get_the_ID();
    $output = get_the_term_list( $postid, 'sermon_speaker');
    
    if ( ! empty ( $output ) )
        return strip_tags ($output); 
}

/**
 * Returns url of record.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_sermon_record_url() {
    
    $postmeta = get_post_meta ( get_the_ID(), 'sermon-record', true );
    
    if ( ! empty ( $postmeta ) )
        return esc_url( $postmeta );
}

/**
 * Returns url of handout.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_sermon_handout_url() {
    
    $postmeta = get_post_meta ( get_the_ID(), 'sermon-handout', true );
    
    if ( ! empty ( $postmeta ) )
        return esc_url( $postmeta );
}

/**
 * Returns sermon player html.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_sermon_player() {

    return efgcp_render_audio_player();
}

/**
 * Returns publishing sermon date.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_sermon_date() {
    
    // Set locality to germany
    setlocale(LC_TIME, 'de_DE', 'de_DE.UTF-8');
    
    // Get value from database
    $datetime = strtotime ( get_post_meta ( get_the_ID(), 'sermon-date', true ) );
    
    // Format data
    if ( $datetime )
        $output = strftime('%d. %B %Y', $datetime);
    
    if ( ! empty ( $output ) )
        return $output;
}

/**
 * Returns assigned sermon topics.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_sermon_topics() {
    
    $postid = get_the_ID();
    $output = get_the_term_list( $postid, 'sermon_topic');
    
    if ( ! empty ( $output ) )
        return strip_tags ($output);
}