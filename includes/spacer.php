<?php

if ( ! defined ( 'ABSPATH' ) ) exit;

function efgcp_space () {
    
    return '<div class="spacer"></div>';
}

add_shortcode('space', 'efgcp_space');