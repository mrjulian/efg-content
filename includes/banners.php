<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

function efgcp_banner ( $atts ) {
    
    // Extract params
    extract( shortcode_atts (
        
        array(
            'image_id'      => '',
            'height'        => '',
            'title_text'    => '',
            'subtitle_text' => '',
            'show_button'   => '',
            'button_text'   => '',
            'button_link'   => ''
        ), $atts )
    );
    
    // Define vars
    $image    = '';
    $title    = '';
    $subtitle = '';
    $button   = '';
    
    // Define banner height
    $img_height = ($height === 'normal') ? 500 : 700;
    
    echo $height;
    
    if ( ! empty ( $image_id ) && is_numeric ( $image_id ) ) {
        
        // Get image
        $image_src = wp_get_attachment_image_src( $image_id, 'full' );
        
        // Print image
        $image = '<img class="banner-image" src=" ' . esc_url ( $image_src[0] ) . ' " height="700">';
    }
    else {
        
        // Print error
        return print_error('$image_id empty or type is not int.');
    }
    
    if ( ! empty ( $title_text ) ) {
        
        // Print title
        $title = '<span class="banner-title title">' . sanitize_text_field( $title_text ) . '</span>';
    }
    
    if ( ! empty ( $subtitle_text ) ) {
        
        // Print subtitle
        $subtitle = '<span class="banner-subtitle subtitle">' . sanitize_text_field( $subtitle_text ) . '</span>';
    }
    
    if ( $show_button && $button_text && $button_link ) {
        
        // Button args
        $btn_title  = sanitize_text_field ( $button_text );
        $btn_target = sanitize_text_field ( vc_build_link($button_link)['target'] );
        $btn_rel    = sanitize_text_field ( vc_build_link($button_link)['rel'] );
        $btn_url    = esc_url ( vc_build_link ( $button_link )['url'] );
        
        // Print button
        $button  = '<div class="banner-button"><button class="button">';
        $button .= '<a href="' . $btn_url . '" target="' . $btn_target . '" rel="' . $btn_rel . '">' . $btn_title . '</a>';
        $button .= '</button></div>';
    }
    
    // Return final banner output
    return '<div class="banner">' . $image . '<div class="banner-inner">' . $title . $subtitle . $button . '</div></div>';
}

add_shortcode( 'banner', 'efgcp_banner' );