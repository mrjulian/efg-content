<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

/**
 * Parent meta box class. Use this class to create meta boxes in the backend.
 *
 * @package     contentpack_backend
 * @since       1.0.0
 */

class EFGCP_Meta_Box {

	public function __construct( $args ) {
        
        $this->meta_box        = $args;
        $this->meta_box_fields = $args['fields'];

		if ( is_admin() ) {
            
			add_action( 'load-post.php', array( $this, 'init_metabox' ) );
			add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
		}
	}

	public function init_metabox() {

		add_action( 'add_meta_boxes', array( $this, 'add_metabox'  ) );
		add_action( 'save_post', array( $this, 'save_metabox' ), 10, 2 );
	}

	public function add_metabox() {

		add_meta_box ( $this->meta_box['id'], $this->meta_box['title'], array( $this, 'render_metabox' ), $this->meta_box['post_type'], $this->meta_box['context'], $this->meta_box['priority'] );
	}

	public function render_metabox( $post ) {

		// Add nonce for security and authentication.
		wp_nonce_field( 'sermon_nonce_action', 'sermon_nonce' );
        
		// Open field table.
		echo '<table class="form-table meta-box-table">';
        
        foreach ( $this->meta_box_fields as $field ) {
            
            // Retrieve an existing value from the database.
            ${ 'existing_' . $field['id'] } = get_post_meta( $post->ID, $field['id'], true );

            // Set default values.
            if ( empty( ${ 'existing_' . $field['id'] } ) ) ${ 'existing_' . $field['id'] } = '';
            
            // Return upload form
            if ( ( $field['type'] == 'datetime' ) )
                require EFGCP_ADM_PATH . '/parts/meta-box-datetime.php';
            
            if ( ( $field['type'] == 'text' ) && ( $field['button'] ) )
                require EFGCP_ADM_PATH . '/parts/meta-box-upload.php';
        }
        
        // Close field table.
		echo '</table>';
	}

	public function save_metabox( $post_id, $post ) {

		// Add nonce for security and authentication.
        if ( isset ($_POST['sermon_nonce'] ) )
            $nonce_name = $_POST['sermon_nonce'];
		
        $nonce_action = 'sermon_nonce_action';

		// Check if a nonce is set.
		if ( ! isset( $nonce_name ) )
			return;

		// Check if a nonce is valid.
		if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
			return;

		// Check if the user has permissions to save data.
		if ( ! current_user_can( 'edit_post', $post_id ) )
			return;

		// Check if it's not an autosave.
		if ( wp_is_post_autosave( $post_id ) )
			return;

		// Check if it's not a revision.
		if ( wp_is_post_revision( $post_id ) )
			return;
        
        foreach ( $this->meta_box_fields as $field ) {
            
            // Sanitize user input.
            ${ 'new_' . $field['id'] } = isset( $_POST[ $field['id'] ] ) ? sanitize_text_field( $_POST[ $field['id'] ] ) : '';
            
            // Update the meta field in the database.
            update_post_meta( $post_id, $field['id'], ${ 'new_' . $field['id'] } );
        }
	}
}