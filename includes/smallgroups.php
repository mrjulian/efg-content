<?php

if ( ! defined ( 'ABSPATH' ) ) exit;

function efgcp_smallgroup ( $atts ) {
    
    // Extract params
    extract ( shortcode_atts(
        array (
            'title_text'       => '',
            'description_text' => '',
            'show_image'       => '',
            'image_id'         => '',
            'show_event'       => '',
            'event_cat'        => '',
            'show_border'      => ''
        ), $atts )
    );
    
    // Define vars
    $title       = '';
    $description = '';
    $image       = '';
    $event       = '';
    $contact     = '<div class="smallgroup-contact"><a href=" ' . get_site_url() . '/kontakt/ansprechpartner">Ansprechpartner</a></div>';
    
    // Setup conditional 
    $border = ($show_border == 'true' ) ? ' border-bottom' : '';
    $column = ($show_image == 'true' ) ? 'vc_col-sm-8' : 'vc_col-12 aligncenter';
    
    // Print title
    if ( ! empty ( $title_text ) ) {
        $title = '<h2 class="smallgroup-title">' . sanitize_text_field( $title_text ) . '</h2>';
    }
    
    // Print description
    if ( ! empty ( $description_text ) ) {
        $description = '<div class="smallgroup-description"><p>' . sanitize_text_field( $description_text ) . '</p></div>';
    }
    
    // Print image
    if ($show_image === 'true' ) {
        
        if ( ! empty ( $image_id ) && is_numeric ( $image_id ) ) {
            
            $image_src = wp_get_attachment_image_src( $image_id, 'medium' );
            
            $image  = '<div class="vc_col-sm-4 aligncenter">';
            $image .= '<img class="smallgroup-image" src=" ' . esc_url ( $image_src[0] ) . ' ">';
            $image .= '</div>';
        }
        else {
            
            $image = print_error('$image_id empty or type is not int.');
        }
    }
    
    // Setup output
    $output  =  '<div class="smallgroup' . $border . '">';
    $output .=      '<div class="vc_row vc_inner vc_row-fluid">';
    $output .=          '<div class="' . $column . '">';
    $output .=              $title. $description . $contact;
    $output .=          '</div>';
    $output .=          $image;
    $output .=      '</div>';
    $output .=  '</div>';
    
    // Return output
    return $output;
};

add_shortcode('smallgroup', 'efgcp_smallgroup');

function efgcp_smallgroup_age ( $atts ) {
    
    // Extract params
    extract ( shortcode_atts(
        array (
            'age_text'   => '',
        ), $atts )
    );
    
    if ( ! empty ( $age_text ) ) {
        
        $output = '<div class="smallgroup-age">' . sanitize_text_field( $age_text ) . '</div>';
    }
    else {

        $output = print_error('$image_id empty or type is not int.');
    }
    
    return $output;
}

add_shortcode('smallgroup-age', 'efgcp_smallgroup_age');
