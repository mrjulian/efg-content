<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

/**
 * Register custom post type 'magazine'
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_register_newsletter() {
    
    $labels = array(
        'name'                => _x( 'G-Profile', 'Post Type General Name', 'efgcp' ),
        'singular_name'       => _x( 'G-Profil', 'Post Type Singular Name', 'efgcp' ),
        'menu_name'           => __( 'G-Profile', 'efgcp' ),
        'name_admin_bar'      => __( 'G-Profil', 'efgcp' ),
        'all_items'           => __( 'Alle ' . 'G-Profile', 'efgcp' )
    );
    
    $rewrite = array(
        'slug'                => 'gprofil',
        'with_front'          => true,
        'pages'               => true,
        'feeds'               => false
    );
    
    $args = array(
        'label'               => __( 'G-Profil', 'efgcp' ),
        'labels'              => $labels,
        'rewrite'             => $rewrite,
        'supports'            => array ( 'title', 'editor', 'thumbnail' ),
        'public'              => true,
        'menu_position'       => 22,
        'menu_icon'           => 'dashicons-book',
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'show_ui'             => true,
        'hierarchical'        => false,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post'
    );
    
    register_post_type( 'magazine', $args );
}

add_action( 'init', 'efgcp_register_newsletter', 16 );

/**
 * Default newsletter query.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function efgcp_newsletter_default_query() {
    
    global $paged;
    
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    $postnum = 18;
        
    $args = array(
        'post_type'      => 'magazine',
        'order'          => 'ASC',
        'orderby'        => 'menu_order',
        'paged'          => $paged,
        'posts_per_page' => $postnum
    );
    
    $output = '';
    
    $posts = new WP_Query( $args );

    if ( $posts->have_posts() ) {
        
        while ( $posts->have_posts() ) {
            
            $posts->the_post();
        
            $output .= efgcp_render_magazine_output();
        }
        
        $total = isset ( $posts->max_num_pages ) ? $posts->max_num_pages : 1;

        $page_args = array(
            'total'     => $total,
            'current'   => $paged,
            'prev_next' => true,
            'prev_text' => __('Prev', 'efg'),
            'next_text' => __('Next', 'efg'),
            'type'      => 'plain',
        );

        $pagination = '<nav class="pagination">' . paginate_links ( $page_args ) . '</nav>';

        return '<div class="post-wrap">' . $output . '</div>' . $pagination;
    }
    
    else {
        
        echo 'Sorry, nothing found.';
    }
    
    wp_reset_postdata();
}

add_shortcode('newsletter_list', 'efgcp_newsletter_default_query');

/**
 * Returns url of newsletter.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_newsletter_url() {
    
    $postmeta = get_post_meta ( get_the_ID(), 'newspaper-download', true );
    $output   = get_the_link ( $postmeta );
    
    if ( ! empty ( $output ) )
        return $output;
}