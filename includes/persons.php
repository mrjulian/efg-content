<?php

if ( ! defined ( 'ABSPATH' ) ) exit;

/**
 * Register custom post type 'person'.
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_register_persons () {
    
    $labels = array(
        'name'           => _x( 'Personen', 'Post Type General Name', 'efgcp' ),
        'singular_name'  => _x( 'Person', 'Post Type Singular Name', 'efgcp' ),
        'menu_name'      => __( 'Personen', 'efgcp' ),
        'name_admin_bar' => __( 'Person', 'efgcp' ),
        'all_items'      => __( 'Alle ' . 'Personen', 'efgcp' )
    );
    
    $rewrite = array(
        'slug'       => 'person',
        'with_front' => true,
        'pages'      => true,
        'feeds'      => false
    );
    
    $args = array(
        'label'               => __( 'Personen', 'efgcp' ),
        'labels'              => $labels,
        'rewrite'             => $rewrite,
        'supports'            => array ( 'title', 'editor', 'thumbnail', 'page-attributes' ),
        'taxonomies'          => array ( 'person_group' ),
        'public'              => true,
        'menu_position'       => 21,
        'menu_icon'           => 'dashicons-groups',
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'show_ui'             => true,
        'hierarchical'        => false,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post'
    );
    
    register_post_type( 'person', $args );
}

add_action( 'init', 'efgcp_register_persons', 14 );

/**
 * Register sermon taxonomy 'person_group'.
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_register_tax_person_group() {
    
    $labels = array(
        'name'          => _x( 'Gruppen', 'Taxonomy General Name', 'efgcp' ),
        'singular_name' => _x( 'Gruppe', 'Taxonomy Singular Name', 'efgcp' ),
        'menu_name'     => __( 'Gruppen', 'efgcp' ),
        'all_items'     => __( 'Alle ' . 'Gruppen', 'efgcp' ),
        'add_new_item'  => __( 'Neue Gruppe erstellen', 'efgcp' )
    );
    
    $rewrite = array(
        'slug'         => 'gruppe',
        'walk_dirs'    => false,
        'with_front'   => true,
        'hierarchical' => true
    );
    
    $args = array(
        'labels'            => $labels,
        'rewrite'           => $rewrite,
        'hierarchical'      => true,
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_tagcloud'     => false
    );
    
    register_taxonomy( 'person_group', 'person', $args );
    
    /* Add default taxonomy term */
    if ( ! term_exists( 'Alle Personen', 'person_group' ) ) {
        wp_insert_term ( 'Alle Personen', 'person_group', array (
            'description' => 'Standard Gruppe. Wird automatisch hinzugefügt.',
            'slug'        => 'alle'
        ) );
    }
}

add_action( 'init', 'efgcp_register_tax_person_group', 15 );

/**
 * Add default taxonomy term everytime a person is saved
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_person_set_default_object_term ( $post_id ) {
    
    $current_post = get_post( $post_id );
    wp_set_object_terms( $post_id, 'Alle Personen', 'person_group', true );
}

add_action( 'save_post_person', 'efgcp_person_set_default_object_term' );

/**
 * Default person query with ability to filter groups.
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_person_default_query( $atts ) {
    
    extract ( shortcode_atts (
        
        array(
            'group'        => ''
        ), $atts )
    );
    
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    
    $tax_query = '';
    
    if ( ! empty ( $group ) ) {
        
        $tax_query = array(
            array(
                'taxonomy' => 'person_group',
                'field'    => 'slug',
                'terms'    => $group,
            )
        );
    }
    
    $args = array(
        'post_type'      => 'person',
        'order'          => 'ASC',
        'orderby'        => 'menu_order',
        'posts_per_page' => 20,
        'tax_query'      => $tax_query
    );
    
    $output = '';
    
    $posts = new WP_Query( $args );

    if ( $posts->have_posts() ) {
        
        while ( $posts->have_posts() ) {
            
            $posts->the_post();
        
            $output .= efgcp_render_person_output();
        }
        
        $total = isset ( $posts->max_num_pages ) ? $posts->max_num_pages : 1;

        $page_args = array(
            'total'     => $total,
            'current'   => $paged,
            'prev_next' => true,
            'prev_text' => __('Prev', 'efg'),
            'next_text' => __('Next', 'efg'),
            'type'      => 'plain',
        );

        $pagination = '<nav class="pagination">' . paginate_links ( $page_args ) . '</nav>';

        return '<div class="post-wrap person-wrap">' . $output . '</div>' . $pagination;
    }
    
    else {
        
        echo 'Sorry, nothing found.';
    }
}

add_shortcode('person_list', 'efgcp_person_default_query');

/**
 * Returns position of person.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_person_position() {
    
    $output = strip_tags ( get_post_meta ( get_the_ID(), 'person_title', true ) );
    
    if ( ! empty ( $output ) )
        return $output;
}

/**
 * Returns mail address of person.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_person_mailaddress() {
    
    $output = strip_tags ( get_post_meta ( get_the_ID(), 'person_email', true ) );
    $output = trim ( $output );
    
    if ( ! empty ( $output ) )
        return '<a href="mailto:' . sanitize_email ( $output ) . '" class="mail-address">E-Mail schreiben</a>';
}

/**
 * Returns phone number of person.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function get_the_person_phonenumber() {
    
    $output = get_post_meta ( get_the_ID(), 'person_phone', true );
    
    if ( ! empty ( $output ) )
        return strip_tags ( $output );
}