<?php

if ( ! defined ( 'ABSPATH' ) ) exit;

function efgcp_add_shortcodes_to_visual_composer () {
    
    if ( function_exists ( 'vc_map') ) {
        
        /**
         * Default sermon query.
         *
         * @since       1.0.0
         * @version     1.0.0
         */
        vc_map (
            array (
                'name'                    => 'Predigten', 'efgcp',
                'base'                    => 'sermon_list',
                'icon'                    => 'fa fa-microphone',
                'show_settings_on_create' => true,
                'description'             => 'Liste mit Predigten.', 'efgcp',
                'params'                  => array (
                    array (
                        'type'                => 'textfield',
                        'heading'             => 'Predigtreihe', 'efgcp',
                        'description'         => 'Zeige nur Predigten aus einer bestimmten Reihe an. Gebe dafür den Slug der Reihe an.</br>Beispiel: back-to-the-roots', 'efgcp',
                        'param_name'          => 'series',
                        'admin_label'         => true,
                    ),
                    array (
                        'type'                => 'textfield',
                        'heading'             => 'Predigtanzahl', 'efgcp',
                        'description'         => 'Wie viele Predigten sollen auf einmal geladen werden?', 'efgcp',
                        'param_name'          => 'postnum',
                        'admin_label'         => true,
                    )
                )
            )
        );

        /**
         * Simple sermon filter.
         *
         * @since       1.0.0
         * @version     1.0.0
         */
        vc_map (
            array (
                'name'                    => 'Predigtfilter', 'efgcp',
                'base'                    => 'sermon_filter_simple',
                'icon'                    => 'fa fa-microphone',
                'show_settings_on_create' => true,
                'description'             => 'Tax Dropdown.', 'efgcp',
                'params'                  => array (
                    array (
                        'type'        => 'dropdown',
                        'heading'     => 'Ausrichten', 'efgcp',
                        'param_name'  => 'align',
                        'value'       => array (
                            'Links'      => 'First Option',
                            'Zentriert'  => 'Second Option',
                            'Rechts'     => 'Third Option',
                          ),
                        'std'         => 'left',
                        'admin_label' => true,
                        'group'       => 'Allgemein',
                    ),
                    array (
                        'type'        => 'checkbox',
                        'heading'     => 'Suchleiste anzeigen', 'efgcp',
                        'param_name'  => 'show_search',
                        'admin_label' => true,
                        'group'       => 'Allgemein',
                    ),
                    array (
                        'type'        => 'textfield',
                        'heading'     => 'Term', 'efgcp',
                        'description' => 'Slug der Kategorie.', 'efgcp',
                        'param_name'  => 'term_1',
                        'group'       => 'Filter 1',
                    ),
                    array (
                        'type'        => 'textfield',
                        'heading'     => 'Label', 'efgcp',
                        'description' => 'Wird angezeigt wenn nichts ausgewählt ist.', 'efgcp',
                        'param_name'  => 'label_1',
                        'group'       => 'Filter 1',
                    ),
                    array (
                        'type'        => 'textfield',
                        'heading'     => 'Slug', 'efgcp',
                        'description' => 'Wird als Teil der URL angezeigt und sollte für Besucher schlüssig sein.', 'efgcp',
                        'param_name'  => 'slug_1',
                        'group'       => 'Filter 1',
                    ),
                    array (
                        'type'        => 'textfield',
                        'heading'     => 'Term', 'efgcp',
                        'description' => 'Slug der Kategorie.', 'efgcp',
                        'param_name'  => 'term_2',
                        'group'       => 'Filter 2',
                    ),
                    array (
                        'type'        => 'textfield',
                        'heading'     => 'Label', 'efgcp',
                        'description' => 'Wird angezeigt wenn nichts ausgewählt ist.', 'efgcp',
                        'param_name'  => 'label_2',
                        'group'       => 'Filter 2',
                    ),
                    array (
                        'type'        => 'textfield',
                        'heading'     => 'Slug', 'efgcp',
                        'description' => 'Wird als Teil der URL angezeigt und sollte für Besucher schlüssig sein.', 'efgcp',
                        'param_name'  => 'slug_2',
                        'group'       => 'Filter 2',
                    )
                )
            )
        );

        /**
         * Advanced sermon filter.
         *
         * @since       1.0.0
         * @version     1.0.0
         */
        vc_map (
            array (
                'name'                    => 'Erweiterte Predigtfilter', 'efgcp',
                'base'                    => 'sermon_filter_advanced',
                'icon'                    => '',
                'show_settings_on_create' => false,
                'description'             => 'Bietet mehr Möglichkeiten, Predigten zu filtern.', 'efgcp'
            )
        );

        /**
         * Default newsletter query.
         *
         * @since       1.0.0
         * @version     1.0.0
         */
        vc_map (
            array (
                'name'                    => 'Gemeinde Unterwegs', 'efgcp',
                'base'                    => 'newsletter_list',
                'icon'                    => '',
                'show_settings_on_create' => false,
                'description'             => 'Gemeinde Unterwegs.', 'efgcp'
            )
        );

        /**
         * Default person query.
         *
         * @since       1.0.0
         * @version     1.0.0
         */

        /* Store person groups to use in Visual Composer */
        $groups = get_terms( 'person_group' );
        $dropdown = array();
        foreach( $groups as $group ){
            $dropdown[ htmlspecialchars_decode ( $group->name ) ] = $group->slug;
        }
        
        vc_map (
            array (
                'name'                    => 'Personen', 'efgcp',
                'base'                    => 'person_list',
                'icon'                    => '',
                'show_settings_on_create' => false,
                'description'             => 'Zeige eine Liste von Personen an.', 'efgcp',
                'params'                  => array (
                    array (
                        'param_name'    => 'group',
                        'type'          => 'dropdown',
                        'value'         => $dropdown,
                        'std'           => 'Alle Personen',
                        'heading'       => __('Gruppenfilter', 'efgcp'),
                        'description'   => __('Zeige nur eine ausgewählte Gruppe von Personen an.', 'efgcp'),
                        'holder'        => 'div',
                        'class'         => ''
                    ),
                )
            )
        );
        
        /**
         * Image banner wih title, subtitle and button.
         *
         * @since       1.0.0
         * @version     1.0.0
         */
        
        vc_map (
            array (
                'name'                    => 'Banner', 'efgcp',
                'base'                    => 'banner',
                'icon'                    => 'fa fa-picture-o',
                'show_settings_on_create' => true,
                'description'             => 'Banner mit Titel und Button.', 'efgcp',
                'params'                  => array (
                    array (
                        'type'          => 'attach_image',
                        'heading'       => 'Background', 'efgcp',
                        'description'   => 'Bildmaße: 1920x700px', 'efgcp',
                        'param_name'    => 'image_id'
                    ),
                    array (
                        'type'          => 'dropdown',
                        'heading'       => 'Bannergröße', 'efgcp',
                        'param_name'    => 'height',
                        'value'         => array (
                            'Normal'    => 'normal',
                            'Größer'    => 'large'
                        )
                    ),
                    array (
                        'type'          => 'textfield',
                        'heading'       => 'Titel', 'efgcp',
                        'param_name'    => 'title_text',
                        'admin_label'   => true
                    ),
                    array (
                        'type'          => 'textfield',
                        'heading'       => 'Untertitel', 'efgcp',
                        'param_name'    => 'subtitle_text',
                        'admin_label'   => false
                    ),
                    array (
                        'type'          => 'checkbox',
                        'heading'       => 'Zeige Button', 'efgcp',
                        'param_name'    => 'show_button',
                        'value'         => array (
                            ''          => 'true'
                        )
                    ),
                    array (
                        'type'          => 'textfield',
                        'heading'       => 'Button Text', 'efgcp',
                        'param_name'    => 'button_text',
                        'dependency'    => array (
                            'element'   => 'show_button',
                            'value'     => 'true'
                        )
                    ),
                    array (
                        'type'          => 'vc_link',
                        'heading'       => 'Button Link', 'efgcp',
                        'param_name'    => 'button_link',
                        'dependency'    => array (
                            'element'   => 'show_button',
                            'value'     => 'true'
                        )
                    )
                )
            )
        );
        
        /**
         * Smallgroup with title, description and image.
         *
         * @since       1.0.0
         * @version     1.0.0
         */
        
        vc_map(
            array (
                'name'                    => 'Kleingruppe', 'efgcp',
                'base'                    => 'smallgroup',
                'icon'                    => 'fa fa-picture-o',
                'show_settings_on_create' => true,
                'description'             => 'Kleingruppe hinzufügen.', 'efgcp',
                'params'                  => array (
                    array (
                        'type'        => 'textfield',
                        'heading'     => 'Titel', 'efgcp',
                        'param_name'  => 'title_text',
                        'admin_label' => true
                    ),
                    array (
                        'type'       => 'textarea',
                        'heading'    => 'Beschreibung', 'efgcp',
                        'param_name' => 'description_text'
                    ),
                    array (
                        'type'       => 'checkbox',
                        'heading'    => 'Bild anzeigen', 'efgcp',
                        'param_name' => 'show_image',
                        'value'      => array (
                            ''       => 'true'
                        )
                    ),
                    array (
                        'type'        => 'attach_image',
                        'heading'     => 'Kleingruppenbild', 'efgcp',
                        'param_name'  => 'image_id',
                        'dependency'  => array (
                            'element' => 'show_image',
                            'value'   => 'true'
                        )
                    ),
                    array (
                        'type'       => 'checkbox',
                        'heading'    => 'Regeltermin anzeigen.', 'efgcp',
                        'param_name' => 'show_event',
                        'value'      => array (
                            ''       => 'true'
                        )
                    ),
                    array (
                        'type'        => 'textfield',
                        'heading'     => 'Veranstaltungskategorie', 'efgcp',
                        'description' => 'Füge hier den Slug der Veranstaltungskategorie ein.', 'efgcp',
                        'param_name'  => 'event_cat',
                        'dependency'  => array (
                            'element' => 'show_event',
                            'value'   => 'true'
                        )
                    )
                )
            )
        );
        
        /**
         * Smallgroup age button.
         *
         * @since       1.0.0
         * @version     1.0.0
         */
        
        vc_map(
            array (
                'name'                    => 'Kleingruppenalter', 'efgcp',
                'base'                    => 'smallgroup-age',
                'icon'                    => 'custom-icon age-icon',
                'show_settings_on_create' => false,
                'description'             => 'Festen Abstand hinzufügen.', 'efgcp',
                'params'                  => array (
                    array (
                        'type'        => 'textfield',
                        'heading'     => 'Text', 'efgcp',
                        'description' => 'Beispiel: "2 - 4 Jahre"', 'efgcp',
                        'param_name'  => 'age_text',
                        'admin_label' => true
                    ),
                )
            )
        );
        
        /**
         * Responsive spacer.
         *
         * @since       1.0.0
         * @version     1.0.0
         */
        
        vc_map(
            array (
                'name'                    => 'Abstandshalter', 'efgcp',
                'base'                    => 'space',
                'icon'                    => 'custom-icon spacer-icon',
                'show_settings_on_create' => false,
                'description'             => 'Festen Abstand hinzufügen.', 'efgcp',
            )
        );
        
        /**
         * Responsive border.
         *
         * @since       1.0.0
         * @version     1.0.0
         */
        
        vc_map(
            array (
                'name'                    => 'Trennlinie', 'efgcp',
                'base'                    => 'border',
                'icon'                    => 'custom-icon border-icon',
                'show_settings_on_create' => false,
                'description'             => 'Trennlinie hinzufügen.', 'efgcp',
            )
        );
    }
}

add_action( 'init', 'efgcp_add_shortcodes_to_visual_composer', 999 );

/**
 * Remove default shortcodes from Visual Composer.
 *
 * @since       1.0.0
 * @version     1.0.0
 */

function efgcp_remove_shortcodes_from_visual_composer () {
    
    if ( function_exists ( 'vc_remove_element') ) {
        
        // Add shortcode names to array
        $shortcodes = array( /*'vc_icon',*/ 'vc_message', 'vc_separator', 'vc_text_separator', 'vc_wp_meta', 'vc_wp_recentcomments', 'vc_wp_search', 'vc_wp_tagcloud', 'vc_wp_text', 'vc_wp_calendar', 'vc_wp_pages', 'vc_wp_posts', 'vc_wp_links', 'vc_wp_categories', 'vc_wp_archives', 'vc_wp_rss', 'vc_custom_heading', 'vc_empty_space', 'vc_clients', 'vc_widget_sidebar', 'vc_masonry_media_grid', 'vc_masonry_grid', 'vc_basic_grid', 'vc_media_grid', 'vc_facebook', 'vc_tweetmeme', 'vc_googleplus', 'vc_pinterest', 'vc_images_carousel', 'vc_button', 'vc_button2', 'vc_cta_button', 'vc_flickr', 'vc_btn', 'vc_cta' );
        
        // Remove shortcodes in array
        foreach ( $shortcodes as $shortcode ) {
            vc_remove_element( $shortcode );
        }
        
        /*$s_elemets = array( 'carousel', 'tour', 'gallery', 'posts_slider', 'posts_grid', 'teaser_grid', 'single_image', 'toogle', , 'cta_button2', 'video', 'gmaps', 'progress_bar', 'raw_js', 'pie' );*/
    }
}

// add_action( 'init', 'efgcp_remove_shortcodes_from_visual_composer', 998 );