<?php

if ( ! defined ( 'ABSPATH' ) ) exit;

function efgcp_border () {
    
    return '<div class="border"></div>';
}

add_shortcode('border', 'efgcp_border');