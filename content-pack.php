<?php
/**
 * Plugin Name:         Content Pack
 * Description:         Adds custom post types and related taxonomies, meta boxes and page templates into WordPress.
 * Version:             1.0.2.1
 * Author:              Julian Wendt
 * Text Domain:         efgcp
 * Domain Path:         /languages
 * GitLab Plugin URI:   https://gitlab.com/jwendt/efg-content-pack
 */

if ( ! defined ( 'ABSPATH' ) ) exit;

/* Define constant path's */
define ( 'EFGCP_PLUGIN_PATH',    plugin_dir_path( __FILE__ ) );
define ( 'EFGCP_ADM_PATH',       plugin_dir_path( __FILE__ ) . '/admin/' );
define ( 'EFGCP_PUB_PATH',       plugin_dir_path( __FILE__ ) . '/public/' );
define ( 'EFGCP_INC_PATH',       plugin_dir_path( __FILE__ ) . '/includes/' );
define ( 'EFGCP_LIB_PATH',       plugin_dir_path( __FILE__ ) . '/library/' );

/* Define constant Url's */
define ( 'EFGCP_PLUGIN_URL',     plugins_url() . '/efg-content-pack/' );
define ( 'EFGCP_ADM_URL',        plugins_url() . '/efg-content-pack/admin/' );
define ( 'EFGCP_INC_URL',        plugins_url() . '/efg-content-pack/includes/' );
define ( 'EFGCP_LIB_URL',        plugins_url() . '/efg-content-pack/library/' );
define ( 'EFGCP_PUB_URL',        plugins_url() . '/efg-content-pack/public/' );

/**
 * Initiate plugin files.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function efgcp_init_files() {
    
    // Load admin files
    foreach ( glob ( EFGCP_PLUGIN_PATH . 'admin/*.php' ) as $file ) {
        
        include_once $file;
    }
    
    // Load remaining includes
    foreach ( glob ( EFGCP_PLUGIN_PATH . 'includes/*.php' ) as $file ) {
        
        include_once $file;
    }
    
    // Load rendered output files
    foreach ( glob ( EFGCP_PLUGIN_PATH . 'public/*.php' ) as $file ) {
        
        include_once $file;
    }
}

add_action( 'init', 'efgcp_init_files' );

/**
 * Enqueue public styles and scripts.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function efgcp_enqueue_public_scripts() {
    
    /* Styles */
    wp_enqueue_style( 'efgcp-style', EFGCP_PUB_URL . '/assets/css/public.min.css' );
}

add_action( 'wp_enqueue_scripts', 'efgcp_enqueue_public_scripts' );

/**
 * Enqueue the admin stylesheet.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function efgcp_enqueue_admin_scripts() {

    wp_enqueue_style( 'efg_content_admin_style', plugins_url() . '/efg-content-pack/admin/assets/css/admin.min.css' );
}

add_action( 'admin_enqueue_scripts', 'efgcp_enqueue_admin_scripts' );

/**
 * Print errors if wp_debug is enabled.
 *
 * @since       1.0.0
 * @version     1.0.0
 */
function print_error ( $error_message ) {
    
    if ( defined('WP_DEBUG') && true === WP_DEBUG )
        return 'Error: ' . $error_message;
}