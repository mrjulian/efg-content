<?php

if ( ! defined ( 'ABSPATH' ) ) exit;

/**
 * Register sermon meta box
 *
 * @version     1.0.0
 * @since       1.0.0
 */
function efgcp_register_sermon_meta_box() {
    
    $fields = array(
        'sermon_date' => array(
            'id'        => 'sermon-date',
            'title'     => 'Veröffentlichen',
            'type'      => 'datetime',
            'desc'      => 'Datum, wann Predigtaufnahme und Handout veröffentlicht werden sollen.',
            'button'    => true
        ),
        'sermon_record' => array(
            'id'        => 'sermon-record',
            'title'     => 'Aufnahme',
            'type'      => 'text',
            'desc'      => 'Upload-Button nutzen um Predigt hochzuladen oder Link einfügen.',
            'button'    => true
        ),
        'sermon_handout' => array(
            'id'        => 'sermon-handout',
            'title'     => 'Handout',
            'type'      => 'text',
            'desc'      => 'Upload-Button nutzen um Handout hochzuladen oder Link einfügen.',
            'button'    => true
        )
    );

    $args = array(
        'id'             => 'sermon-media',
        'title'          => 'Predigt Medien',
        'post_type'      => 'sermon',
        'context'        => 'advanced',
        'priority'       => 'default',
        'fields'         => $fields
    );

    new EFGCP_Meta_Box ( $args );
}

add_action( 'init', 'efgcp_register_sermon_meta_box', 14 );