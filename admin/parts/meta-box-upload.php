<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit;

?>

<tr>
    
    <th>
        
        <label for="<?php echo esc_attr( $field['id']) ; ?>" class="<?php echo esc_attr( $field['id'] ); ?>-label"><?php echo esc_textarea(__( $field['title'], 'efgcp' ) ); ?></label>
    
    </th>
    
    <td>
        
        <input type="<?php echo esc_attr( $field['type']); ?>" id="<?php echo esc_attr($field['id']); ?>" name="<?php echo esc_attr($field['id'] ); ?>" class="textfield"
               value="<?php echo esc_textarea( ${ 'existing_' . $field['id'] } ); ?>">
        
        <input id="upload-button" type="button" class="<?php echo esc_attr( $field['id'] ) . '-button'; ?> button" value="Upload" />
        
        <p class="description"><?php echo esc_textarea( $field['desc'] ); ?></p>
        
        <script type="text/javascript">
        
            var jQuery;

            jQuery(document).ready(function ($) {
                
                'use strict';
                var mediaUploader;
                
                $('.meta-box-table .<?php echo $field['id'] . '-button'; ?>').click(function (e) {
                    e.preventDefault();
                    
                    // If the uploader object has already been created, reopen the dialog
                    if (mediaUploader) {
                        mediaUploader.open();
                        return;
                    }
                    
                    // Extend the wp.media object
                    mediaUploader = wp.media.frames.file_frame = wp.media({
                        title: 'Choose Image',
                        button: {
                            text: 'Choose Image'
                        },
                        multiple: false
                    });

                    // When a file is selected, grab the URL and set it as the text field's value
                    mediaUploader.on('select', function () {
                        var attachment = mediaUploader.state().get('selection').first().toJSON();
                        $('#<?php echo $field['id'] ?>').val(attachment.url);
                    });
                    
                    // Open the uploader dialog
                    mediaUploader.open();
                });
                
            });
        
        </script>
        
    </td>
    
</tr>