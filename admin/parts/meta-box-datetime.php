<?php

// No direct access!
if ( ! defined ( 'ABSPATH' ) ) exit; ?>

<tr>
    <th>    
        <label for="<?php esc_attr( $field['id'] ); ?>'" class="<?php esc_attr( $field['id'] ); ?>'_label"><?php echo esc_textarea( __( $field['title'], 'efgcp' ) ); ?></label>
    </th>
    <td>
        <input type="datetime" id="<?php echo esc_attr( $field['id'] ); ?>" name="<?php echo esc_attr( $field['id'] ); ?>" class="datetime" placeholder="01.01.2017 14:00"
               value="<?php echo esc_textarea( ${ 'existing_' . $field['id'] } ); ?>">
        <p class="description"><?php echo esc_textarea( $field['desc']); ?></p>
    </td>
</tr>