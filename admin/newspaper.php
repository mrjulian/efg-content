<?php

if ( ! defined ( 'ABSPATH' ) ) exit;

function efgcp_register_newspaper_meta_box() {
    
    $fields = array(
        'newsletter_download' => array(
            'id'        => 'newsletter-download-url',
            'title'     => 'Datei (PDF)',
            'type'      => 'text',
            'desc'      => 'Das ist eine Beschreibung.',
            'button'    => true
        )
    );

    $args = array(
        'id'             => 'newspaper-media',
        'title'          => 'Medien',
        'post_type'      => 'magazine',
        'context'        => 'advanced',
        'priority'       => 'default',
        'fields'         => $fields
    );

    new EFGCP_Meta_Box ( $args );
}

add_action( 'init', 'efgcp_register_newspaper_meta_box', 15 );