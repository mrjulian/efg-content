<?php

// Register the column
function register_order_column( $columns ) {

    $columns['menu_order'] = 'Reihenfolge';
    return $columns;
}

add_filter( 'manage_edit-persons_columns', 'register_order_column' );

// Display the column content
function display_column_content($name) {

    global $post;

    switch ($name) {

        case 'menu_order': $order = $post->menu_order;
        
        echo $order;
        break;

        default: break;
   }
}

add_action( 'manage_persons_posts_custom_column' , 'display_column_content' );

// Register the column as sortable
function make_order_column_sortable($columns) {

    $columns['menu_order'] = 'menu_order';

    return $columns;
}

add_filter( 'manage_edit-persons_sortable_columns' , 'make_order_column_sortable' );

// Hide columns
function hide_post_columns( $columns ) {

    unset($columns['date']);

    return $columns;
}

add_filter( 'manage_edit-persons_columns' , 'hide_post_columns' , 10, 1 );